package pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.internal.WebElementToJsonConverter;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;

public class HomePage_CA extends BasePage{

	//Top Bar Navigation items
	private By newArrivals_link;
	private By women_link;
	private By men_link;
	private By accessories_link;
	private By love21_link;
	private By girls_link;
	private By plusSizes_link;
	private By sale_link;
	private By lookbooks_link;
	
	//Footer links
	private By aboutUS_link = By.id("FooterAboutUsLink");
	private By careers_link = By.id("FooterCareersLink");
	private By affiliateProgram_link = By.id("FooterReturnPolicyLink");
	private By socialResponsability_link = By.id("FooterSocialResponsibilityLink");
	private By businessWithUs = By.id("FooterBusinessWithUsLink");
	
	private By contactUs_link = By.id("FooterContactUsLink");
	private By trackOrder_link = By.id("FooterCheckOrderStatusLink");
	private By FAQs_link = By.id("FooterFAQsLink");
	private By shippingInfo_link = By.id("FooterShippingInfoLink");
	private By giftCards_link = By.id("FooterGiftCardsLink");
	private By sizeGuide_link = By.id("FooterSizeGuideLink");
	private By myAccount_link = By.id("FooterMyAccountLink");
	private By storeLocator_link = By.id("FooterStoreLocatorLink");
	
	
	
	
	private ArrayList linksTopNavigation = new ArrayList();
	private ArrayList footerLinks = new ArrayList();
	
	
	
	
	public  HomePage_CA (WebDriver _driver){
		super(_driver);	
		
		newArrivals_link = By.linkText("NEW ARRIVALS");
		getAllLinksTopNavigation().add(newArrivals_link);
		women_link = By.xpath("//li[contains(.,'Women')]");
		//getAllLinksTopNavigation().add(women_link);
		men_link  = By.linkText("MEN");
		getAllLinksTopNavigation().add(men_link);
		accessories_link  = By.linkText("ACCESSORIES");
		getAllLinksTopNavigation().add(accessories_link);
		love21_link  = By.linkText("LOVE21");
		getAllLinksTopNavigation().add(love21_link);
		girls_link  = By.linkText("GIRLS");
		getAllLinksTopNavigation().add(girls_link);
		plusSizes_link  = By.linkText("PLUS SIZES");
		getAllLinksTopNavigation().add(plusSizes_link);
		sale_link  = By.linkText("SALE");
		getAllLinksTopNavigation().add(sale_link);
		lookbooks_link  = By.linkText("LOOKBOOKS");
		getAllLinksTopNavigation().add(lookbooks_link);
		
		try{
			//Closes the banner
			WebElement closeLink = driver.findElement(By.cssSelector("#signup > div.dijitDialogTitleBar > span.dijitDialogCloseIcon > span"));
			if(closeLink.isDisplayed() && closeLink != null){
				new Actions(driver).moveToElement(closeLink).click().perform();	
			}
		}
		catch(Exception e){
			
		}
		
		
	
	}
	

	public ArrayList getAllLinksTopNavigation() {
		return linksTopNavigation;
	}

	public ArrayList getFooterLinks() {
		footerLinks.add(aboutUS_link);
		footerLinks.add(careers_link);
		footerLinks.add(affiliateProgram_link);
		footerLinks.add(socialResponsability_link);
		footerLinks.add(businessWithUs);
		footerLinks.add(contactUs_link);
		footerLinks.add(trackOrder_link);
		footerLinks.add(FAQs_link);
		footerLinks.add(shippingInfo_link);
		footerLinks.add(giftCards_link);
		footerLinks.add(sizeGuide_link);
		footerLinks.add(myAccount_link);
		footerLinks.add(storeLocator_link);
		
		return footerLinks;
	}





	





	
}
