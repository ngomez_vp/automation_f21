package pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import sun.awt.geom.AreaOp.AddOp;

public class GiftCardPage_CA extends BasePage {
	
	private By productRightCarousel = By.id("product1_rr");
	private By productMainImage = By.id("productMainImage");
	private By offerPrice = By.xpath("//*[contains(@id, 'offerPrice')]");
	private By facetsList = By.xpath("//*[contains(@id, 'swatch_-')]");
	private By qtyList = By.xpath("quantity_");
	private By addToBag = By.id("productPageAdd2Cart");
	private By starsChart = By.className("pr-stars");
	private By reviewsLink = By.id("pr-snippet-read-reviews");
	private By socialMedia = By.className("widget_social_bottom");
	private By description = By.id("tab1_link");
	private WebElement topBarCart;
	private By qvCloseLink = By.id("WC_QuickInfo_Link_close");
	
	//Input fields for e-cards
	
	private By recipientName = By.id("E-GIFT_TO");
	private By senderName = By.id("E-GIFT_FROM");
	private By recipientMail = By.id("EGIFT_RECEIVERS_EMAIL");
	private By senderMail = By.id("EGIFT_SENDERS_EMAIL");
	private By confirmRecipient = By.id("EGIFT_CONFIRMED_RECEIVERS_EMAIL");
	private By message = By.id("EGIFT_MESSAGE");
	private By previewLink = By.id("addToShoppingList");
	
	//Validation span's and stuff
	private By selectAmount_msg = By.linkText("Please Select an Amount");
	private By closeMsg = By.id("cancel");
	
	public boolean ValidateGCFormatPage(String gcType) throws Exception{
		
		boolean itemsPresent=false;
		ArrayList<WebElement> items = new ArrayList<WebElement>();
		
		try{
			
			if (gcType.toString().equals("physical")){
				items.add(driver.findElement(productRightCarousel));
				items.add(driver.findElement(productMainImage));
				items.add(driver.findElement(offerPrice));
				items.add(driver.findElement(facetsList));
				items.add(driver.findElement(qtyList));
				items.add(driver.findElement(addToBag));
				items.add(driver.findElement(starsChart));
				items.add(driver.findElement(reviewsLink));
				items.add(driver.findElement(socialMedia));
				items.add(driver.findElement(description));
				
				for(int i=0; i<items.size(); i++){
					WebElement tmp = items.get(i);
					itemsPresent = tmp.isDisplayed();
				}	
			}
			else if(gcType.toString().equals("virtual")){
				items.add(driver.findElement(productRightCarousel));
				items.add(driver.findElement(productMainImage));
				items.add(driver.findElement(offerPrice));
				items.add(driver.findElement(qtyList));
				items.add(driver.findElement(addToBag));
				
				items.add(driver.findElement(recipientName));
				items.add(driver.findElement(senderName));
				items.add(driver.findElement(recipientMail));
				items.add(driver.findElement(senderMail));
				items.add(driver.findElement(confirmRecipient));
				items.add(driver.findElement(message));
				items.add(driver.findElement(previewLink));
				
				
				for(int i=0; i<items.size(); i++){
					WebElement tmp = items.get(i);
					itemsPresent = tmp.isDisplayed();
				}	
			}
			
			
			
		}
		catch(Exception e){
			itemsPresent = false;
			throw e;
		}
		return itemsPresent;
		
	}

	public void Validate_PhysicalGCQuickView(){
		WebElement checkout;
		WebElement modifyLink;
		WebElement addToBag_lnk; 
		List<WebElement> priceFacet;
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		WebDriverWait wait= new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.presenceOfElementLocated(topBarcart_lnk));
		
		priceFacet = driver.findElements(facetsList);
		
		priceFacet.get(0).click();
		addToBag_lnk = driver.findElement(addToBag);
		addToBag_lnk.click();
		
		topBarCart = driver.findElement(topBarcart_lnk);
		new Actions(driver).moveToElement(topBarCart).perform();
		executor.executeScript("arguments[0].click();", topBarCart);
	    checkout = (new WebDriverWait(driver, 60)).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='shopcartCheckout']/div")));
	    modifyLink = driver.findElement(By.linkText("Modify"));
	    modifyLink.click();
	    WebElement qvClose =(new WebDriverWait(driver, 60)).until(ExpectedConditions.elementToBeClickable(By.id("WC_QuickInfo_Link_close")));
	    qvClose.click();
	    
	
	}
	
//must be reviewed later	
	public void Validate_EGC_QuickView(){
		WebElement checkout;
		WebElement modifyLink;
		WebElement addToBag_lnk; 
		WebElement recipient_name;
		WebElement sender_name;
		WebElement recipient_mail;
		WebElement sender_mail;
		WebElement confirm_recipient;
		WebElement message_txt;
		List<WebElement> priceFacet;
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		WebDriverWait wait= new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.presenceOfElementLocated(topBarcart_lnk));
		
		priceFacet = driver.findElements(facetsList);
		
		priceFacet.get(1).click();
		recipient_name = driver.findElement(recipientName);
		recipient_name.sendKeys("random recipient name");
		sender_name = driver.findElement(senderName);
		sender_name.sendKeys("random name");
		recipient_mail = driver.findElement(recipientMail);
		recipient_mail.sendKeys("randomMail@randomHost.com");
		sender_mail = driver.findElement(senderMail);
		sender_mail.sendKeys("senderMail@randomHost.com");
		confirm_recipient = driver.findElement(confirmRecipient);
		confirm_recipient.sendKeys("randomMail@randomHost.com");
		message_txt = driver.findElement(message);
		message_txt.sendKeys("this is a random message");
		
		executor.executeScript("document.getElementById('productPageAdd2Cart').click();");
		
		topBarCart = driver.findElement(topBarcart_lnk);
		new Actions(driver).moveToElement(topBarCart).perform();
		executor.executeScript("arguments[0].click();", topBarCart);
	    checkout = (new WebDriverWait(driver, 60)).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='shopcartCheckout']/div")));
	    modifyLink = driver.findElement(By.linkText("Modify"));
	    modifyLink.click();
	    WebElement qvClose =(new WebDriverWait(driver, 60)).until(ExpectedConditions.elementToBeClickable(By.id("WC_QuickInfo_Link_close")));
	    qvClose.click();

		
	}
	
	public void EGC_ValidateMandatoryFields(){
		WebElement bag;
		WebElement cancelErrormsg;
		WebDriverWait wait;
		List<WebElement> facets = driver.findElements(facetsList);
		
		facets.get(0).click();
		bag = driver.findElement(addToBag);
		bag.click();
		cancelErrormsg = driver.findElement(closeMsg);
		wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.presenceOfElementLocated(closeMsg));
		new Actions(driver).moveToElement(cancelErrormsg).click().perform();

		
		
		
		try{
			Alert alert = driver.switchTo().alert();
			alert.accept();
			/*JavascriptExecutor executor = (JavascriptExecutor) driver;
			Object p = executor.executeScript("(window.opener != null)? true : false;");*/
			System.out.println(alert.getText());
		}
		catch(Exception e){
			
		}
	
	}
	
	
	public  GiftCardPage_CA (WebDriver _driver){
		super(_driver);
			
	}
	
}
