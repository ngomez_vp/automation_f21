package pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import business.NewUser;
import sun.awt.geom.AreaOp.AddOp;

public class SignIn_Register_CA extends BasePage {
	
	private By first_name = By.xpath("//*[contains(@id, 'WC__NameEntryForm_FormInput_firstName_')]");
	private By last_name = By.xpath("//*[contains(@id, 'WC__NameEntryForm_FormInput_lastName_')]");
	private By year_dropDwn = By.id("WC_PersonalInfoExtension_birth_year");
	private By month_dropDwn = By.id("WC_PersonalInfoExtension_birth_month");
	private By day_dropDwn = By.id("WC_PersonalInfoExtension_birth_date");
	private By _email = By.xpath("//*[contains(@id, 'WC_UserRegistrationAddForm_FormInput_logonId_In_Register_')]");
	private By confirm_email = By.xpath("//*[contains(@id, 'WC_UserRegistrationAddForm_FormInput_logonIdVerify_In_Register_')]");
	private By _password = By.xpath("//*[contains(@id, 'WC_UserRegistrationAddForm_FormInput_logonPassword_In_Register_')]");
	private By confirm_password = By.xpath("//*[contains(@id, 'WC_UserRegistrationAddForm_FormInput_logonPasswordVerify_In_Register_')]");
	private By forgotPassword_lnk = By.xpath("//*[contains(@id, 'WC_AccountDisplay_div_')]/p/a");
	private By email_txt = By.className("fieldStyle");
	private By submit_forgotPwd = By.xpath("//*[contains(@id, 'WC_PasswordResetForm_Link_')]");
	private By forgotPwd_confirmMsg = By.xpath("//*[contains(@id, 'WC_PasswordResetDisplay_div_')]");
	
	

	public void RegisterNewUser(NewUser user){
		
		WebElement firstName = driver.findElement(first_name);
		WebElement lastName = driver.findElement(last_name);
		WebElement yearOfBirth = driver.findElement(year_dropDwn);
		WebElement monthOfBirth = driver.findElement(month_dropDwn);
		WebElement dayOfBirth = driver.findElement(day_dropDwn);
		WebElement email = driver.findElement(_email);
		WebElement confirmEmail = driver.findElement(confirm_email);
		WebElement password = driver.findElement(_password);
		WebElement confirmPassword = driver.findElement(confirm_password);
	
		
		firstName.sendKeys(user.getFirstName());
		lastName.sendKeys(user.getLastName());
		
		Select clickThis = new Select(yearOfBirth);
		clickThis.selectByVisibleText(user.getYOB());
		clickThis = new Select(monthOfBirth);
		clickThis.selectByVisibleText(user.getMOB());
		clickThis = new Select(dayOfBirth);
		clickThis.selectByVisibleText(user.getDOB());
		
		email.sendKeys(user.getEmail());
		confirmEmail.sendKeys(user.getConfirEmail());
		password.sendKeys(user.getPassword());
		confirmPassword.sendKeys(user.getConfirmPassword());
		
		
		
	
		
	}

	public void ForgotPasswordProcedure(NewUser user){
		
		
		WebElement forgotPwd = driver.findElement(forgotPassword_lnk);
		forgotPwd.click();
		WebElement waiter = (new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(forgotPwd_confirmMsg));
		WebElement txtEmail = driver.findElement(email_txt);
		txtEmail.sendKeys(user.getResetMail().toString());
		WebElement submitLnk = driver.findElement(submit_forgotPwd);
		submitLnk.click();
		
		
	}
	
	public  SignIn_Register_CA (WebDriver _driver){
		super(_driver);
			
	}
	
}
