package pages;

import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;



public class ShoppingCartPage_CA extends BasePage {

	Properties properties;
	

	
	//Paypal User Data
	private By emailPayPal_txt= By.id("login_email");
	private By pwdPayPal_txt=By.id("login_password");
	private By loginPayPal_lnk=By.id("submitLogin");
	WebElement emailPP;
	WebElement pwdPP;
	WebElement loginPP;

	
	public ShoppingCartPage_CA(WebDriver driver){
		super(driver);
		
		
		
	}
	

	public void ProceedToCheckoutPayPal() throws IOException{
		emailPP = driver.findElement(emailPayPal_txt);
		pwdPP = driver.findElement(pwdPayPal_txt);
		loginPP = driver.findElement(loginPayPal_lnk);
		
		properties = new Properties();
		properties.load(getClass().getResourceAsStream("pages.properties"));
		
		String tmpUsr = properties.getProperty("ppUser").toString();
		String tmpPwd = properties.getProperty("ppPwd").toString();
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript ("document.getElementById('method-paypal').focus()");
			
		emailPP.sendKeys(tmpUsr);
		pwdPP.sendKeys(tmpPwd);
		loginPP.click();
		WebElement continueLnk = (new WebDriverWait(driver, 30)).until(ExpectedConditions.elementToBeClickable(By.id("continue_abovefold")));
		continueLnk.click();
		
		
		
	}
}
