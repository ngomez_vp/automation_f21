package pages;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import helpers.Utils;

import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ShippingBillingPage_CA extends BasePage {
	
	//Shipping & Address fields
	
	private By firstName = By.id("firstName");
	private By lastName = By.id("lastName");
	private By streetAddress = By.id("address1");
	private By city = By.id("city");
	private By state = By.id("state");
	private By zipCode = By.id("zipCode");
	private By phone = By.id("phone1");
	private By saveShipAddress = By.id("saveShipAddressInput");
	
	//Credit Card fields
	
	private By checkbox_chk=By.id("addrPrimary");
	private By checkout_lnk=By.xpath(("//*[@id='shopcartCheckout']/img"));
	private By sameAsShipping_chk=By.id("sameAsShippingRadio");
	private By cvcField_txt = By.id("cardPin");
	private By placeOrder_lnk = By.className("proceedCheckoutBut_large");
	private By gcNumber_txt = By.id("giftcard_number");
	private By gcPin_txt = By.id("giftcard_pin");
	private By applyGc_btn = By.id("applyGCBtn");
	private WebElement btnContinue;
	WebElement sameAs;
	Select countryShipList;
	Select stateList;

	
	//Validations span's
	private By cc_cvcError = By.id("cc_cvcErrorDiv");
	private By cc_cardNameError=By.id("cc_cardNameErrorDiv");
	private By cc_emptyCardNumberError=By.id("cc_emptyCardNumberErrorDiv");
	private By cc_ExpirationDateError=By.id("cc_ExpirationDateErrorDiv");
	
		
	public WebElement getBtnContinue() {
		btnContinue = driver.findElement(checkout_lnk);
		return btnContinue;
	}
	
	public WebElement getSameShippingAddresCheck(){
		sameAs = driver.findElement(sameAsShipping_chk);
		return sameAs;
	}



	



	public ShippingBillingPage_CA(WebDriver _driver){
		super(_driver);
		
		
	}
	
	
	
	public void ShippingAdressAndMethod(){
		 WebElement checkbox=driver.findElement(checkbox_chk);
		 btnContinue = driver.findElement(checkout_lnk);
		 checkbox.click();
		 getBtnContinue().click();
		 
		 
	}
	
	public void EnterBillingInfoCC(boolean gc, boolean egc){
		
		sameAs = driver.findElement(sameAsShipping_chk);
		WebElement cvcText = driver.findElement(cvcField_txt);
		sameAs.click();
		cvcText.sendKeys("123");
		EnterGiftCard(gc, egc);
		
		JavascriptExecutor js = (JavascriptExecutor)driver;
        js.executeScript("window.scrollTo(0,Math.max(document.documentElement.scrollHeight," + "document.body.scrollHeight,document.documentElement.clientHeight));");
        btnContinue = driver.findElement(By.xpath("//*[@id='multiPageCheckoutContinue']/img"));
        new Actions(driver).moveToElement(getBtnContinue()).click().perform();
        
        
	}
	
	private void EnterGiftCard(boolean gc, boolean egc){
		WebElement gcNumber = driver.findElement(gcNumber_txt);
		WebElement gcPin = driver.findElement(gcPin_txt);
		WebElement btnApplyGc = driver.findElement(applyGc_btn);
		
		Properties properties = new Properties();
		try {
			properties.load(getClass().getResourceAsStream("pages.properties"));
			if (gc){
				gcNumber.sendKeys(properties.getProperty("gc_cc").toString());
				gcPin.sendKeys(properties.getProperty("gc_pin").toString());
				btnApplyGc.click();
				WebElement btnApply = (new WebDriverWait(driver, 60)).until(ExpectedConditions.elementToBeClickable(applyGc_btn));
			}
			
			if(egc){
				gcNumber.sendKeys(properties.getProperty("egc_cc").toString());
				gcPin.sendKeys(properties.getProperty("egc_pin").toString());
				btnApplyGc.click();
				WebElement btnApply = (new WebDriverWait(driver, 60)).until(ExpectedConditions.elementToBeClickable(applyGc_btn));
			}
			
			WebElement btnApply = (new WebDriverWait(driver, 60)).until(ExpectedConditions.elementToBeClickable(applyGc_btn));
			
			
			
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	
		//if (properties.getProperty("browser").toString().equals("FIREFOX"))
			
			
	}
	
	public boolean ValidateCanadaShipping(){
		
		boolean validCombo;
		
		countryShipList = new Select(driver.findElement(By.id("country")));
		validCombo = !(countryShipList.isMultiple());
		validCombo = countryShipList.getFirstSelectedOption().getText().toString().equals("Canada");
		return validCombo;
		
	}
	
	public boolean ValidaMandatoryShippingAddressFields(){
		
		boolean validationsRaised = false;
		
		WebElement firstN = driver.findElement(firstName);
		WebElement lastN = driver.findElement(lastName);
		WebElement address = driver.findElement(streetAddress);
		WebElement city_txt = driver.findElement(city);
		WebElement state_txt = driver.findElement(state);
		WebElement zipCode_txt = driver.findElement(zipCode);
		WebElement phone_txt = driver.findElement(phone);
		WebElement saveButton = driver.findElement(saveShipAddress);
		WebElement error_msg;
		WebElement error_msg2;
		String error_msg_txt;
		String error_msg2_txt;
		
		firstN.sendKeys("");
		saveButton.click();
		error_msg=  driver.findElement(By.className("dijitTooltipContents"));
		error_msg_txt = error_msg.getText();
		validationsRaised = (error_msg_txt.length() > 0 );
		
		firstN.sendKeys("Nicolas");
		lastN.sendKeys("");
		saveButton.click();
		error_msg2=  driver.findElement(By.className("dijitTooltipContents"));
		error_msg2_txt = error_msg2.getText();
		validationsRaised = (error_msg2_txt.length() > 0 && error_msg_txt != error_msg2_txt);
		
		firstN.sendKeys("");
		lastN.sendKeys("Gomez");
		saveButton.click();
		error_msg=  driver.findElement(By.className("dijitTooltipContents"));
		error_msg_txt = error_msg.getText();
		validationsRaised = ( error_msg_txt.length() > 0 && error_msg_txt != error_msg2_txt);
		
		firstN.sendKeys("Nicolas");
		lastN.clear();
		lastN.sendKeys("Gomez");
		saveButton.click();
		error_msg2=  driver.findElement(By.className("dijitTooltipContents"));
		error_msg2_txt = error_msg2.getText();
		validationsRaised = ( error_msg2_txt.length() > 0 && error_msg_txt != error_msg2_txt );
		
		
		firstN.clear();
		lastN.clear();
		firstN.sendKeys("Nicolas");
		lastN.sendKeys("Gomez");
		address.sendKeys("Address One");
		saveButton.click();
		error_msg=  driver.findElement(By.className("dijitTooltipContents"));
		error_msg_txt = error_msg.getText();
		validationsRaised = ( error_msg_txt.length() > 0 && error_msg_txt != error_msg2_txt );
		
		firstN.clear();
		lastN.clear();
		address.clear();
		firstN.sendKeys("Nicolas");
		lastN.sendKeys("Gomez");
		address.sendKeys("Address Two");
		saveButton.click();
		error_msg2=  driver.findElement(By.className("dijitTooltipContents"));
		error_msg2_txt = error_msg2.getText();
		validationsRaised = ( error_msg2_txt.length() > 0 && error_msg_txt != error_msg2_txt );
		
		
		firstN.clear();
		lastN.clear();
		address.clear();
		firstN.sendKeys("Nicolas");
		lastN.sendKeys("Gomez");
		address.sendKeys("Address Two");
		city_txt.sendKeys("City");
		saveButton.click();
		error_msg=  driver.findElement(By.className("dijitTooltipContents"));
		error_msg_txt = error_msg.getText();
		validationsRaised = ( error_msg_txt.length() > 0 && error_msg_txt != error_msg2_txt );
		
		
		firstN.clear();
		lastN.clear();
		address.clear();
		city_txt.clear();
		firstN.sendKeys("Nicolas");
		lastN.sendKeys("Gomez");
		address.sendKeys("Address Two");
		city_txt.sendKeys("HIGH PRAIRIE");
		phone_txt.sendKeys("+1 204-318-1418");
		saveButton.click();
		error_msg2=  driver.findElement(By.className("dijitTooltipContents"));
		error_msg2_txt = error_msg2.getText();
		validationsRaised = ( error_msg2_txt.length() > 0 && error_msg_txt != error_msg2_txt );
		
		
		firstN.clear();
		lastN.clear();
		address.clear();
		city_txt.clear();
		phone_txt.clear();
		firstN.sendKeys("Nicolas");
		lastN.sendKeys("Gomez");
		address.sendKeys("Address Two");
		city_txt.sendKeys("HIGH PRAIRIE");
		phone_txt.sendKeys("+1 204-318-1418");
		zipCode_txt.sendKeys("T0G 1E0");
		saveButton.click();
		error_msg=  driver.findElement(By.className("dijitTooltipContents"));
		error_msg_txt = error_msg.getText();
		validationsRaised = ( error_msg_txt.length() > 0 && error_msg_txt != error_msg2_txt );
		
		
		firstN.clear();
		lastN.clear();
		address.clear();
		city_txt.clear();
		phone_txt.clear();
		firstN.sendKeys("Nicolas");
		lastN.sendKeys("Gomez");
		address.sendKeys("Address Two");
		city_txt.sendKeys("HIGH PRAIRIE");
		phone_txt.sendKeys("+1 204-318-1418");
		zipCode_txt.sendKeys("T0G 1E0");
		saveButton.click();
		error_msg2=  driver.findElement(By.className("dijitTooltipContents"));
		error_msg2_txt = error_msg2.getText();
		validationsRaised = ( error_msg2_txt.length() > 0 && error_msg_txt != error_msg2_txt );
		System.out.println(error_msg2_txt);	

		return validationsRaised;	
		
	}
	
	public void CreateShippingAddress(){
		
		WebElement firstN = driver.findElement(firstName);
		WebElement lastN = driver.findElement(lastName);
		WebElement address = driver.findElement(streetAddress);
		WebElement city_txt = driver.findElement(city);
		WebElement state_txt = driver.findElement(state);
		WebElement zipCode_txt = driver.findElement(zipCode);
		WebElement phone_txt = driver.findElement(phone);
		WebElement saveButton = driver.findElement(saveShipAddress);
		
		firstN.sendKeys("Nicolas");
		lastN.sendKeys("Gomez");
		address.sendKeys("Address Two");
		city_txt.sendKeys("HIGH PRAIRIE");
		phone_txt.sendKeys("+1 204-318-1418");
		zipCode_txt.sendKeys("T0G 1E0");
		stateList = new Select(driver.findElement(state));
		stateList.selectByVisibleText("Alberta");
		saveButton.click();
		
		
		
	}
	
	
	public int PlaceOrder(){
		int orderId=-1;
		WebElement placeOrder = (new WebDriverWait(driver, 30)).until(ExpectedConditions.elementToBeClickable(placeOrder_lnk));
        placeOrder.click();
        

        WebElement orderId_txt = driver.findElement(By.id("WC_OrderShippingBillingConfirmationPage_span_1"));
        
        orderId = Integer.parseInt(orderId_txt.getText());
        
        return orderId;
	}
	
	public void ValidateMandatoryCCFields(String field){
		
		boolean validationRaised;
		sameAs = driver.findElement(sameAsShipping_chk);
		//First, CVC not present
		
		WebElement tmp=null;
        
        if(field.toString().equals("ccNumber")){
        	
        }
        if(field.toString().equals("cvc")){
        	btnContinue = driver.findElement(By.xpath("//*[@id='multiPageCheckoutContinue']/img"));
            new Actions(driver).moveToElement(getBtnContinue()).click().perform();
            tmp= driver.findElement(cc_cvcError);
            
        
        	
        }
        if(field.toString().equals("month")){
        	
        }
        if(field.toString().equals("year")){
        	
        }
        if(field.toString().equals("name")){
        	
        }
        
        if(tmp.isDisplayed() && tmp!= null){
        	validationRaised = true;
        }
        

        
		/*cc_cardNameError;
		cc_emptyCardNumberError;
		cc_ExpirationDateError;
		
		return validationRaised;*/
		
	}

}
