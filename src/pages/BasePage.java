package pages;

import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class BasePage {

	protected static WebDriver driver;
	protected By topBarcart_lnk = By.id("minishopcart_total");
	Properties properties;
	
	
	
	public BasePage(WebDriver _driver){
		 driver = _driver;
		 properties = new Properties();
		
		
		try {
			properties.load(getClass().getResourceAsStream("pages.properties"));
		} catch (IOException e) {
		
			e.printStackTrace();
		}
	}
	
	public BasePage(){
		
	}
}
