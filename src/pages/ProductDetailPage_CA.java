package pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import tests.BaseTest.PaymentMethod;

public class ProductDetailPage_CA extends BasePage{

	private By size_lnk;
	private By quantity_lnk;
	private By addToCart_lnk;
	private By soldOut_lnk;
	
	
	private int orderId=-1;
	
	public int SelectSizeAddToCart( String qty, String sizeId , PaymentMethod method, boolean gc, boolean egc, boolean singleItem) throws IOException, InterruptedException{
		
		size_lnk = By.id(sizeId);
		//quantity_lnk = By.id(id);
		addToCart_lnk = By.id("productPageAdd2Cart");
		soldOut_lnk = By.id("oosSoldOutMsg_1");
		WebElement soldOut = driver.findElement(soldOut_lnk);
		
		if (soldOut.isDisplayed()){
			return -1;
		}
		
		WebElement addToCart = driver.findElement(addToCart_lnk);
		WebElement size = driver.findElement(size_lnk);
		size.click();
		addToCart.click();
		//WebElement quantity = driver.findElement(By.id(quantity_lnk));
		
		/*WebElement cart = driver.findElement(cart_lnk);
		new Actions(driver).moveToElement(cart).perform();
		
		WebDriverWait wait= new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.presenceOfElementLocated(cart_lnk));
	    driver.findElement(cart_lnk).click();
		
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", cart);*/
		
		
	
		if(singleItem == true) {
			WebElement topBarcart = driver.findElement(topBarcart_lnk);
			new Actions(driver).moveToElement(topBarcart).perform();
			
			WebDriverWait wait= new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.presenceOfElementLocated(topBarcart_lnk));
		    driver.findElement(topBarcart_lnk).click();
			
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", topBarcart);
			if(method.equals(PaymentMethod.CC) && method !=null ){
				orderId= BillingInfoCC(gc, egc);			
			}
			if(method.equals(PaymentMethod.PayPal) && method !=null ){
				orderId= BillingInfoPayPal();
			}
		}
		
		return orderId;
	}
	
	
	private int BillingInfoPayPal() throws InterruptedException, IOException  {
		
		
		int orderId= -1;
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		
		WebElement checkoutPP = (new WebDriverWait(driver, 60)).until(ExpectedConditions.elementToBeClickable(By.className("paypal")));
		executor.executeScript("arguments[0].click();", checkoutPP);
		
		WebElement waitLogin2 = (new WebDriverWait(driver, 60)).until(ExpectedConditions.elementToBeClickable(By.id("login_email")));
		ShoppingCartPage_CA shopCartPage = PageFactory.initElements(driver, ShoppingCartPage_CA.class);
		shopCartPage.ProceedToCheckoutPayPal();
		
	
		WebElement deliveryMethod = (new WebDriverWait(driver, 60)).until(ExpectedConditions.elementToBeClickable(By.id("deliveryMethodRadio")));
		ShippingBillingPage_CA shipBillPage = PageFactory.initElements(driver, ShippingBillingPage_CA.class);
		deliveryMethod.click();
		
		
		return shipBillPage.PlaceOrder();
	}


	public int BillingInfoCC(boolean gc, boolean egc) throws IOException{
		
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		
		WebElement checkout = (new WebDriverWait(driver, 60)).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='shopcartCheckout']/div")));
		executor.executeScript("arguments[0].click();", checkout);
		//checkout.click();
		
		WebElement waitLogin = (new WebDriverWait(driver, 60)).until(ExpectedConditions.elementToBeClickable(By.id("WC_CheckoutLogonf_div_4")));
		
		TopBarPage_CA topBarPage = new TopBarPage_CA(driver);
		topBarPage.CheckoutUser();
		
		WebElement ShipBillPage = (new WebDriverWait(driver, 60)).until(ExpectedConditions.elementToBeClickable(By.id("deliveryMethodRadio")));
		ShippingBillingPage_CA shipBillPage = PageFactory.initElements(driver, ShippingBillingPage_CA.class);
		shipBillPage.ShippingAdressAndMethod();
		
		shipBillPage.EnterBillingInfoCC(gc, egc);
		
	
		return shipBillPage.PlaceOrder();
		
	}
	
	
	
	
	
	public ProductDetailPage_CA(WebDriver _driver){
		super(_driver);
	
		
	}
}
