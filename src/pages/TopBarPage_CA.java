package pages;

import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public final class TopBarPage_CA{

	private static By email_lnk = By.id("logonId");
	private static By password_lnk= By.name("logonPassword");
	private static By checkoutUser_lnk = By.id("guestShopperLogon");
	private static By login_lnk = By.id("WC_AccountDisplay_links_2");
	//private static By bag_lnk = By.id("minishopcart_total");
	private static By bag_lnk = By.className("cart");
	
	static WebElement checkoutUser;
	static WebElement email;
	static WebElement password;
	static WebElement signUp;
	static WebElement bag; 
	static Properties properties;
	static WebDriver driver;
	
	//Search items
	private By searchTextbox_input = By.id("SimpleSearchForm_SearchTerm");
	private By submitSearch_input = By.id("search_submit");
	
	public static By getBag_lnk() {
		return bag_lnk;
	}




	public By getSearchTextbox_input() {
		return searchTextbox_input;
	}


	public By getSubmitSearch_input() {
		return submitSearch_input;
	}
	
	public void SignUp() throws IOException{
		
		email= driver.findElement(email_lnk);
		password = driver.findElement(password_lnk);
		properties = new Properties();
		properties.load(getClass().getResourceAsStream("pages.properties"));
		String tmpUsr = properties.getProperty("username").toString();
		String tmpPwd = properties.getProperty("password").toString();
		
		email.sendKeys(tmpUsr);
		password.sendKeys(tmpPwd);
		signUp= driver.findElement(login_lnk);
		new Actions(driver).moveToElement(signUp).click().perform();
		
	}
	
	public void CheckoutUser() throws IOException{
		
		email= driver.findElement(email_lnk);
		password = driver.findElement(password_lnk);
		properties = new Properties();
		properties.load(getClass().getResourceAsStream("pages.properties"));
		String tmpUsr = properties.getProperty("username").toString();
		String tmpPwd = properties.getProperty("password").toString();
		checkoutUser = driver.findElement(checkoutUser_lnk);
		
		email.sendKeys(tmpUsr);
		password.sendKeys(tmpPwd);
		new Actions(driver).moveToElement(checkoutUser).click().perform();
		
		}
	
	public void ClickBagLink(){
		bag = driver.findElement(getBag_lnk());
		this.bag.click();
	}
	
	public  TopBarPage_CA (WebDriver _driver) throws IOException{
		driver = _driver;
		properties = new Properties();
		properties.load(getClass().getResourceAsStream("pages.properties"));
		
		
		
			
	}
	
}
