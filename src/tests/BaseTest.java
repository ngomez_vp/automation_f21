package tests;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.omg.CORBA.portable.InputStream;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.IReporter;
import org.testng.ISuite;
import org.testng.ISuiteResult;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import business.NewUser.*;

public class BaseTest extends TestListenerAdapter{

	public static WebDriver driver;
	public enum PaymentMethod {CC,PayPal}
	JavascriptExecutor executor;
	private int m_count = 0;

	
	//Instantiates the driver instance
	@BeforeMethod
	protected void beforeTest() throws IOException{
		
		Properties properties = new Properties();
		properties.load(getClass().getResourceAsStream("test1_data.properties"));
	
		if (properties.getProperty("browser").toString().equals("FIREFOX")) {

			DesiredCapabilities capability = DesiredCapabilities.firefox();
			capability.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			driver = new FirefoxDriver(capability);
		}	
		if (properties.getProperty("browser").toString().equals("CHROME")) {
			System.setProperty("webdriver.chrome.driver", "C:/chromedriver.exe");
			DesiredCapabilities capability = DesiredCapabilities.chrome();
			capability.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			driver = new ChromeDriver();
			//this.driver = new RemoteWebDriver("http://localhost:9515", DesiredCapabilities.chrome());
		}
		if (properties.getProperty("browser").toString().equals("IE") ) {
			System.setProperty("webdriver.ie.driver", "C:/IEDriverServer.exe");
			driver = new InternetExplorerDriver();
		}
		 
		executor = (JavascriptExecutor)driver;
		driver.manage().window().maximize();
		driver.navigate().to(properties.getProperty("baseUrl"));
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("widget_minishopcart")));
		
	}
	
	
	@AfterMethod
	public void getRunTime(ITestResult tr) {
	    long time = tr.getEndMillis() - tr.getStartMillis();
	    System.out.println("Test : " + tr.getName().toString() + "**" + " Time (in milis): " + time);
	    
	}
	//Operations performed after a test is executed
	@AfterMethod
	protected void afterTest(){
		
		
        driver.close();
	}
	
	
	 @Override
	 public void onTestFailure(ITestResult tr) {
	    log(tr.toString());
	 }
	 
	 protected void log(String string) {
		    System.out.print(string);
		    if (++m_count % 40 == 0) {
		      System.out.println("");
		    }
		  }


}
