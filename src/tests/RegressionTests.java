package tests;


import static org.junit.Assert.*;
import helpers.Utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import business.NewUser;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;

import pages.GiftCardPage_CA;
import pages.HomePage_CA;
import pages.ProductDetailPage_CA;
import pages.SearchResultsPage_CA;
import pages.ShippingBillingPage_CA;
import pages.SignIn_Register_CA;
import pages.TopBarPage_CA;
import sun.util.logging.resources.logging;

public class RegressionTests extends BaseTest{

	
	HomePage_CA homePage;
	ProductDetailPage_CA pdpPage;
	TopBarPage_CA topBarPage;
	
	ArrayList<String> sizesMultiplePurchase = new ArrayList<String>();
	List<WebElement> products;
	List<WebElement> gitfCards;
	WebElement productWaiter;
	WebElement join;
	WebElement signIn;
	WebElement textField;
	WebElement submitLink;
	
	//**Category endpoints
	private String endpoint_womenActiveWear = "/women-Women/apparel-Clothing/women-activewear"; 
	private String endpoint_womenSwimwear = "/women-Women/apparel-Clothing/swimwear-all-Swimwear"; 
	private String endpoint_accessoriesGC = "/women-accessories-gift-cards";
	private String endpoint_menShoes = "/men-shoes";
	private String endpoint_shopByOutfit = "/women-Women/apparel-Clothing/women-shop-by-outfit";
	private String endpoint_belts = "/women-accessories-belts";
	
	
	//**Data Providers for some of the tests. 
	// Please also take into account that, for each @Test that consumes a given array, each instance will be executed for each array record.
	
	
	
	@DataProvider(name = "gc_egc")
	public static Boolean[][] gc_egc() {	
        return new Boolean[][] { { true, false }, { false, true }, { true, true} , { false, false} };
        //return new Boolean[][] { { false, false}  };
		
	}
	@DataProvider(name = "validSearchStrings")
	public static Object[][] validSearchStr() {
		
        return new Object[][] {{ "LACE TOP"} };
	}
	@DataProvider(name = "invalidSearchStrings")
	public static Object[][] invalidSearchStr() {
		
        return new Object[][] { {"SDFGSDFG"}};
	}
	@DataProvider(name = "categoryLinkTexts")
	public static Object[][] categoryLinkText() {
		
        return new Object[][] { {"LOVE21", "LOVE 21"}};
	}
	@DataProvider(name = "womenCategoryLink")
	public static Object[][] womenCategoryLinkText() {
		
        return new Object[][] { {"WOMEN", "Actiwear"}};
	}
	@DataProvider(name = "gcTypes")
	public static Object[][] gcTypes() {
		
        return new Object[][] { {"physical", 0} , {"virtual", 1} };
        
	}
	
	//**End DataProviders
	
	@Test
	public void ToNavigationAppearance(){
		
		HomePage_CA page = PageFactory.initElements(driver, HomePage_CA.class);
		ArrayList navigationLinks = page.getAllLinksTopNavigation();
		
		WebElement tmp;
		
		for (int i=0; i < navigationLinks.size(); i++) {
			By tmpBy = (By) navigationLinks.get(i);
			
			try {
				 tmp = driver.findElement(tmpBy) ;
				 
				 if (i>0){
					 //COmparison of the y axe (location) between the links
					 By tmpComparison = (By) navigationLinks.get(i-1);
					 WebElement weComparison = driver.findElement(tmpComparison);
				 		if (tmp.getLocation().x < weComparison.getLocation().x){
				 		assertTrue(false);
				 		}
				 }
				 
			}
			catch (ElementNotFoundException e){
				assertTrue(false);
			}
				
		}
		
	}

	@Test
	public void HomePageFooterLinks(){
		
		//scroll down to the bottom of the page
		HomePage_CA page = PageFactory.initElements(driver, HomePage_CA.class);
		try {			
			//Scroll to the bottom of the page
			
            executor.executeScript("window.scrollTo(0,Math.max(document.documentElement.scrollHeight," + "document.body.scrollHeight,document.documentElement.clientHeight));");
            
            ArrayList footerLinks = page.getFooterLinks();
            
            for (int i=0; i < footerLinks.size(); i++) {
            	
            	
            	assertTrue(driver.findElement((By) footerLinks.get(i)).isDisplayed());
            }
            
            
		} catch (Exception e) {
			
			System.out.println(e.getMessage());
		}
		
		
		
	}
	
	@Test(dataProvider = "validSearchStrings")
	public void ValidHomePageSearch(String search_str) throws IOException{
		
		HomePage_CA page = PageFactory.initElements(driver, HomePage_CA.class);
		topBarPage = new TopBarPage_CA(driver);
		WebElement textField = driver.findElement(topBarPage.getSearchTextbox_input());
		WebElement submitLink = driver.findElement(topBarPage.getSubmitSearch_input());
		WebElement refineBy_widget;
		
		new Actions(driver).moveToElement(textField).click().perform();
		textField.sendKeys(search_str);
		
		submitLink.click();
		WebElement searchResults = (new WebDriverWait(driver, 60)).until(ExpectedConditions.elementToBeClickable(By.id("searchTotalCount")));
		assertTrue(searchResults.isDisplayed());
		refineBy_widget = driver.findElement(By.id("productsFacets"));
		assertTrue(refineBy_widget.isDisplayed());
		WebElement searchResultsMsg = driver.findElement(By.id("search_results_msg"));
		assertTrue(searchResultsMsg.isDisplayed());
		
	}
	
	public void SearchSingleProduct(String searchText, int productIndex) throws IOException{
		
		homePage = PageFactory.initElements(driver, HomePage_CA.class);
		
		topBarPage = new TopBarPage_CA(driver);
		textField = (new WebDriverWait(driver, 60)).until(ExpectedConditions.elementToBeClickable(topBarPage.getSearchTextbox_input()));
		submitLink = driver.findElement(topBarPage.getSubmitSearch_input());	
		new Actions(driver).moveToElement(textField).click().perform();
		new Actions(driver).moveToElement(submitLink).perform();
		textField.sendKeys(searchText);
		submitLink.click();
		
		WebElement searchResultsMsg = (new WebDriverWait(driver, 60)).until(ExpectedConditions.elementToBeClickable(By.id("search_results_msg")));
		SearchResultsPage_CA searchResultsPage = PageFactory.initElements(driver, SearchResultsPage_CA.class);
		
		products = driver.findElements(By.className("product"));
		
		new Actions(driver).moveToElement(products.get(productIndex)).click().perform();
		WebElement addtoCart = (new WebDriverWait(driver, 60)).until(ExpectedConditions.elementToBeClickable(By.className("current")));
		//WebElement addtoCart = (new WebDriverWait(driver, 60)).until(ExpectedConditions.elementToBeClickable(By.id("productPageAdd2Cart")));
		pdpPage = PageFactory.initElements(driver, ProductDetailPage_CA.class);
		
		
	
	}
	
	@Test(dataProvider = "gc_egc")
	public void BasicPurchaseCC(boolean gc, boolean egc) throws IOException{
		
		SearchSingleProduct("Sweetest Valentine Sweatshirt",0);
		
		//Beginning of the whole purchase process
		int orderId=-1;
		try {
			orderId= pdpPage.SelectSizeAddToCart("1", "swatch_-M_1", PaymentMethod.CC, gc, egc , true);
			
			
		} catch (IOException e) {
			
			System.out.println(e.getStackTrace());
			assertTrue(false);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		assertTrue("Order was not created", orderId!=-1);
				
	}
	
	@Test
	public void BasicPuchasePayPal() throws IOException{

		
		SearchSingleProduct("Sweetest Valentine Sweatshirt",0);
		
		//Beginning of the whole purchase process
		int orderId=-1;
		try {
			orderId= pdpPage.SelectSizeAddToCart("1", "swatch_-M_1", PaymentMethod.PayPal, false,false, true);
			
			
		} catch (IOException e) {
			
			System.out.println(e.getStackTrace());
			assertTrue(false);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		assertTrue("Order was not created", orderId!=-1);
		
		
		
		
	}

	@Parameters
	@Test
	public void MultipleItemsPurchase() throws IOException, InterruptedException{
		int orderId=-1;
		sizesMultiplePurchase.add("swatch_-M_1");
		sizesMultiplePurchase.add("swatch_-M_1");
		sizesMultiplePurchase.add("facet_-31_1");
			
		SearchSingleProduct("Buffalo Plaid Short Sleeve Shirt",0);
		pdpPage.SelectSizeAddToCart("1", sizesMultiplePurchase.get(0), PaymentMethod.CC, false, false , false);
		driver.navigate().back();
		WebElement waitLogin = (new WebDriverWait(driver, 60)).until(ExpectedConditions.elementToBeClickable(By.id("searchTotalCount")));
		
			try {
				
				 for(int i=1; i<sizesMultiplePurchase.size()-1; i++){
					 products = driver.findElements(By.className("product"));
					 new Actions(driver).moveToElement(products.get(i)).click().perform();
					 WebElement addtoCart = (new WebDriverWait(driver, 60)).until(ExpectedConditions.elementToBeClickable(By.className("current")));
					 pdpPage.SelectSizeAddToCart("1", sizesMultiplePurchase.get(i), PaymentMethod.CC, false, false , false);
					 driver.navigate().back();
					 waitLogin = (new WebDriverWait(driver, 60)).until(ExpectedConditions.elementToBeClickable(By.id("searchTotalCount")));
					 
					 
				 }
				products = driver.findElements(By.className("product"));
				new Actions(driver).moveToElement(products.get(sizesMultiplePurchase.size()-1)).click().perform();
				WebElement addtoCart = (new WebDriverWait(driver, 60)).until(ExpectedConditions.elementToBeClickable(By.className("current")));
				orderId= pdpPage.SelectSizeAddToCart("1", sizesMultiplePurchase.get(sizesMultiplePurchase.size()-1), PaymentMethod.CC, false, false , true);
				
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		
	}
	
	@Test(dataProvider = "invalidSearchStrings")
	public void InvalidHomePageSearch(String search_str) throws IOException{
		
		HomePage_CA page = PageFactory.initElements(driver, HomePage_CA.class);
		
		topBarPage = new TopBarPage_CA(driver);
		WebElement textField = driver.findElement(topBarPage.getSearchTextbox_input());
		WebElement submitLink = driver.findElement(topBarPage.getSubmitSearch_input());
		
		new Actions(driver).moveToElement(textField).click().perform();
		textField.sendKeys(search_str);
		
		submitLink.click();
		WebElement searchResults = (new WebDriverWait(driver, 60)).until(ExpectedConditions.elementToBeClickable(By.className("emptySearchResultSpot")));
		WebElement carousel = (new WebDriverWait(driver, 60)).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[contains(@id, 'prod_PP')]")));
		assertTrue(searchResults.isDisplayed());
		List<WebElement> carouselPopularPicks = driver.findElements(By.xpath("//*[contains(@id, 'prod_PP')]"));
		
		WebElement popularPicks = driver.findElement(By.id("productsResultTab"));
		WebElement newArrivals = driver.findElement(By.id("contentsResultTab"));
		WebElement alt_popularPicks = driver.findElement(By.className("active_tab"));
		WebElement price = driver.findElement(By.className("productblock-price"));
		
		assertTrue(popularPicks.isDisplayed()); 
		assertTrue(popularPicks.equals(alt_popularPicks)); //Check that 'Popular Picks' is defaulted
		assertTrue(driver.findElement(By.id("contentsResultTab")).isDisplayed());
		assertTrue(carouselPopularPicks.size() == 20); //20 items should be displayed in 'Popular Picks' list
		assertTrue(price.isDisplayed());
		
		newArrivals.click();
		List<WebElement> carouselNewArrivals = driver.findElements(By.xpath("//*[contains(@id, 'prod_NA')]"));
		assertTrue(carouselNewArrivals.size() == 20); //20 items should be displayed in 'New Arrivals' list
		carouselNewArrivals.get(0).click();
		pdpPage = PageFactory.initElements(driver, ProductDetailPage_CA.class);
	}

	@Test(dataProvider = "validSearchStrings")
	public void CurrencyCheckforCA(String search_str) throws IOException{
		
		String pageSize;
		Map<String, String> queryParams;
		
		HomePage_CA page = PageFactory.initElements(driver, HomePage_CA.class);
		topBarPage = new TopBarPage_CA(driver);
		WebElement textField = driver.findElement(topBarPage.getSearchTextbox_input());
		WebElement submitLink = driver.findElement(topBarPage.getSubmitSearch_input());
		new Actions(driver).moveToElement(textField).click().perform();
		textField.sendKeys(search_str);
		
		submitLink.click();
		WebElement searchResults = (new WebDriverWait(driver, 60)).until(ExpectedConditions.elementToBeClickable(By.id("searchTotalCount")));
		
		//List<WebElement> priceTags_EN= driver.findElements(By.className("product_price"));
		List<WebElement> priceTags_EN = driver.findElements(By.xpath("//*[contains(@id, 'product_price')]"));
		URL url = new URL(driver.getCurrentUrl());
		queryParams = Utils.splitQuery(url);
		pageSize = queryParams.get("pageSize");
		assertTrue(priceTags_EN.size() == Integer.parseInt(pageSize)); //Ensure there's one price per product
		
		for (int x = 0; x < priceTags_EN.size(); x++) {
			 String tmp = priceTags_EN.get(x).getText();
			 assertTrue(tmp.contains("CAD"));
	    }
		 
		executor.executeScript("javascript:switchLanguageCurrency('SetLanguageCurrencyPreferenceForm', '-1002');");
		searchResults = (new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(By.id("searchTotalCount")));
		
		List<WebElement> priceTags_FR= driver.findElements(By.xpath("//*[contains(@id, 'product_price')]"));
		queryParams = Utils.splitQuery(url);
		pageSize = queryParams.get("pageSize"); 
		
		for(int j = 0; j < priceTags_FR.size(); j++) {
			assertTrue(priceTags_FR.get(j).getText().contains("CAD"));
	    	}
		
		}
	
	@Test(dataProvider = "categoryLinkTexts")
	public void CategoryPage_ProductInfo(String linkText, String catName){
		
		homePage = PageFactory.initElements(driver, HomePage_CA.class);
		WebElement catLink = driver.findElement(By.linkText(linkText));
		catLink.click();
		WebElement searchResults = (new WebDriverWait(driver, 60)).until(ExpectedConditions.elementToBeClickable(By.className("sorting_sizechart")));
		
		List<WebElement> products = driver.findElements(By.className("product_info"));
		
		for (int x = 0; x < products.size(); x++) {
			String temp = products.get(x).getText();
			String lines[] = temp.split("\\r?\\n");
			assertTrue(lines[0].equals(catName));
			assertTrue(lines[1].length() > 0);
			assertTrue(lines[2].matches("CAD \\$\\d+(.\\d{2})?"));
		}	
	}
		
	@Test(dataProvider = "categoryLinkTexts")
	public void CategoryPage_SizeChart(String linkText, String catName) throws InterruptedException{
		
		homePage = PageFactory.initElements(driver, HomePage_CA.class);
		WebElement catLink = driver.findElement(By.linkText(linkText));
		catLink.click();
		WebElement searchResults = (new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(By.className("sorting_sizechart")));
		WebElement sizeChart = driver.findElement(By.className("sorting_sizechart"));
		sizeChart.click();
		Thread.sleep(3000);
		WebElement sizingHead = (new WebDriverWait(driver, 90)).until(ExpectedConditions.presenceOfElementLocated(By.className("closeText")));
		assertTrue(sizingHead.isEnabled());
				
		System.out.println();
		
	}
	
	@Test(dataProvider = "womenCategoryLink")
	public void CategoryPage_WomenActiveWear(String linkText, String catName) throws InterruptedException{
		
		
		driver.navigate().to(driver.getCurrentUrl()+endpoint_womenActiveWear); //In PROD, this differs
		
		WebElement searchResults = (new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(By.className("sorting_fitguide")));
		WebElement fitGuide = driver.findElement(By.className("sorting_fitguide"));
		new Actions(driver).moveToElement(fitGuide).click().perform();
		fitGuide.click();
		Thread.sleep(3000);
		WebElement sizingHead = (new WebDriverWait(driver, 90)).until(ExpectedConditions.presenceOfElementLocated(By.className("closeText")));
		assertTrue(sizingHead.isEnabled());
				
		System.out.println();
		
	}
	
	@Test
	public void CategoryPageWomen_SwimWearBundlePDP(){
		
		driver.navigate().to(driver.getCurrentUrl()+endpoint_womenSwimwear); //In PROD, this differs
		
		WebElement productWaiter = (new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(By.className("product")));
		List<WebElement> products = driver.findElements(By.className("product"));
		
		products.get(0).click();
		WebElement bundle =(new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(By.id("widget_product_info_viewer")));
		List<WebElement> twoBundles = driver.findElements(By.id("widget_product_info_viewer"));
		assertTrue(twoBundles.size() == 2);
		List<WebElement> productOptions = driver.findElements(By.className("product_options"));
		assertTrue(productOptions.size() == 2);
		
		
	}
	
	

	@Test
	public void CategoryPageWomen_SwimWearBundle_QuickView() throws InterruptedException{
		
		driver.navigate().to(driver.getCurrentUrl()+endpoint_womenSwimwear); //In PROD, this differs
		
		WebElement productWaiter = (new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(By.className("product")));
		Thread.sleep(3000);
		//executor.executeScript("function showIt() {document.querySelector('[id^=\"QuickViewLink_\"]').style.visibility = \"hidden\";}setTimeout(\"showIt()\", 2500);");
		WebElement qvLink = driver.findElement(By.xpath("//*[contains(@id, 'QuickViewLink')]/span"));
		new Actions(driver).moveToElement(qvLink).click().perform();
		qvLink.click();
		
		System.out.println("");
		/*List<WebElement> products = driver.findElements(By.className("product"));
		
		products.get(0).click();
		WebElement bundle =(new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(By.id("widget_product_info_viewer")));
		List<WebElement> twoBundles = driver.findElements(By.id("widget_product_info_viewer"));
		assertTrue(twoBundles.size() == 2);
		List<WebElement> productOptions = driver.findElements(By.className("product_options"));
		assertTrue(productOptions.size() == 2);*/
		
		
	}
	

	@Test
	public void QVOverview(){
		
	driver.navigate().to(driver.getCurrentUrl()+"/girls-dresses"); //In PROD, this differs
	productWaiter = (new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(By.className("product")));
	List<WebElement> products = driver.findElements(By.className("product"));
	
	Actions action = new Actions(driver);
	action.moveToElement(products.get(0)).moveToElement(products.get(0)).click().build().perform();
	
	//The following XPATH belongs to the first item of the results page
	
	WebElement qv = driver.findElement(By.xpath("//*[@id=\"searchBasedNavigation_widget\"]/span/div[2]/div/div/div/div/div[2]/fieldset/div/div[2]/div[1]"));
	action.moveToElement(qv).moveToElement(qv).click().build().perform();
	
	System.out.println(qv.getText());
	
	}
	
	
	@Test
	public void ShoesItemCheckCatPage(){
		
		driver.navigate().to(driver.getCurrentUrl()+endpoint_menShoes);
		productWaiter = (new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(By.className("product")));
		List<WebElement> shoeSize_Selectors = driver.findElements(By.className("swatch"));
		products = driver.findElements(By.xpath("//*[contains(@id, 'catalogEntry_image')]"));
				
		ShoeSizeOrderChecker(shoeSize_Selectors);
		 
		products.get(0).click();
		WebElement addtoCart = (new WebDriverWait(driver, 60)).until(ExpectedConditions.elementToBeClickable(By.className("current")));
		pdpPage = PageFactory.initElements(driver, ProductDetailPage_CA.class);
		
		System.out.println("");
		
		shoeSize_Selectors = driver.findElements(By.className("swatch"));
		ShoeSizeOrderChecker(shoeSize_Selectors);
		
			
	}
	
	private void ShoeSizeOrderChecker(List<WebElement> shoeSize_Selectors){
		
		for(int i=0; i<shoeSize_Selectors.size(); i++){
			 
		 	String next_str="";
		 	String prev_str="";
		 	int next = 1;
		 	int prev = 0;
		 	
		 	if(shoeSize_Selectors.size() < i){
		 		next_str = shoeSize_Selectors.get(i+1).getText();
		 	}
		 		prev_str = shoeSize_Selectors.get(i).getText();
		 	
		 	
		 	if(next_str.length() > 0 && prev_str.length() > 0){
		 		next = Integer.parseInt(next_str);
		 		prev = Integer.parseInt(prev_str);
		 		assertTrue(prev < next);
		 	}
	 
	}
	}
	
	
	@Test(dataProvider = "gcTypes")
	public void Accessories_GiftCards(String type, int index){
		
		driver.navigate().to(driver.getCurrentUrl()+ endpoint_accessoriesGC); 
		productWaiter = (new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(By.id("imgNextGift")));
		
		assertFalse(driver.findElement(By.id("widget_left_nav")).isDisplayed());
		
		
		gitfCards = driver.findElements(By.id("imgNextGift"));
		gitfCards.get(index).click();
		
		productWaiter = (new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(By.className("pr-snippet-stars")));
		GiftCardPage_CA gcPage = PageFactory.initElements(driver, GiftCardPage_CA.class);
		try {
			assertTrue(gcPage.ValidateGCFormatPage(type));
		} catch (Exception e) {
			log(e.getStackTrace().toString());
		}
		
		
	
		
	}
	

	
	@Test
	public void Accessories_PhysicalGiftCardQV(){
		driver.navigate().to(driver.getCurrentUrl()+ endpoint_accessoriesGC); 
		productWaiter = (new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(By.id("imgNextGift")));
		gitfCards = driver.findElements(By.id("imgNextGift"));
		gitfCards.get(0).click();
		
		productWaiter = (new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(By.className("pr-snippet-stars")));
		GiftCardPage_CA gcPage = PageFactory.initElements(driver, GiftCardPage_CA.class);
		gcPage.Validate_PhysicalGCQuickView();
	}

	
	@Test
	public void Accessories_EGC_MissingFields(){
		driver.navigate().to(driver.getCurrentUrl()+ endpoint_accessoriesGC); 
		productWaiter = (new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(By.id("imgNextGift")));
		gitfCards = driver.findElements(By.id("imgNextGift"));
		gitfCards.get(1).click();
		
		productWaiter = (new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(By.className("pr-snippet-stars")));
		GiftCardPage_CA gcPage = PageFactory.initElements(driver, GiftCardPage_CA.class);
		gcPage.EGC_ValidateMandatoryFields();
		
	}
	
	
	@Test
	public void Accesories_E_GiftCard_QV(){
		
		driver.navigate().to(driver.getCurrentUrl()+ endpoint_accessoriesGC); 
		productWaiter = (new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(By.id("imgNextGift")));
		gitfCards = driver.findElements(By.id("imgNextGift"));
		gitfCards.get(1).click();
		
		productWaiter = (new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(By.className("pr-snippet-stars")));
		GiftCardPage_CA gcPage = PageFactory.initElements(driver, GiftCardPage_CA.class);
		gcPage.Validate_EGC_QuickView();
		
	}
	
	
	@Test
	public void CategoryPage_SBO() throws InterruptedException{
		
		WebElement next;
		List<WebElement> carouselVisibleElements;
		List<WebElement> chg_carouselVisibleElements;
		
		driver.navigate().to(driver.getCurrentUrl()+ endpoint_shopByOutfit);
		productWaiter = (new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[contains(@id, 'product_price_')]")));
		
		assertTrue(driver.findElement(By.className("widget_carousel")).isDisplayed()); //Top Carousel
		carouselVisibleElements = driver.findElements(By.xpath("//*[contains(@id, 'MiniGridViewProdImg_')]"));
		
		for (int i=0; i < carouselVisibleElements.size(); i++) {
			System.out.println(carouselVisibleElements.get(i).getAttribute("src"));
		}
		
		
		
		next = driver.findElement(By.id("navNext"));
		next.click();
		WebDriverWait wait = new WebDriverWait(driver, 5000); 
		wait.until(ExpectedConditions.elementToBeClickable((By.xpath("//*[contains(@id, 'MiniGridViewProdImg_')]")))); 
		chg_carouselVisibleElements = driver.findElements(By.xpath("//*[contains(@id, 'MiniGridViewProdImg_')]"));
		if(carouselVisibleElements.size() == chg_carouselVisibleElements.size()){
			ArrayList tmpGen= new ArrayList();
			tmpGen.add(carouselVisibleElements);
			System.out.println("First round: " + carouselVisibleElements.size());
			tmpGen.add(chg_carouselVisibleElements);
			System.out.println("Second round: " + chg_carouselVisibleElements.size());
			Set intersect = Utils.intersection(tmpGen);
			System.out.println(intersect.size());
			
		}
		
		
		
	}


	@Test
	public void CategoryPage_OneSizeItem() throws Exception{
		
		List<WebElement> sizeFacets;
		
		driver.navigate().to(driver.getCurrentUrl()+ endpoint_belts);
		productWaiter = (new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[contains(@id, 'product_price_')]")));

		products = driver.findElements(By.xpath("//*[contains(@id, 'catalogEntry_image')]"));
		new Actions(driver).moveToElement(products.get(0)).click().perform();
		productWaiter = (new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(By.className("size_list")));
		
		sizeFacets = driver.findElements(By.xpath("//*[contains(@id, 'facetLabel_')]"));
		
		if( ! (sizeFacets.size()>1) ){
			assertTrue(sizeFacets.get(0).getText().toString().equals("ONE SIZE"));
		}
		else {
			throw new Exception("Selected item has more than one size");
		}
		
		
	}

	
	@Test
	public void Register(){
		
		join = driver.findElement(By.id("accountSpan"));
		join.click();
		productWaiter = (new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[contains(@id, 'WC_AccountDisplay_')]")));
		
		SignIn_Register_CA 	 regPage = PageFactory.initElements(driver, SignIn_Register_CA.class);
		NewUser newUser = new NewUser();
		regPage.RegisterNewUser(newUser);
		executor.executeScript("document.getElementsByClassName(\"button_secondary\")[1].click();");
		productWaiter = (new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[contains(@id, 'WC_MyAccountDisplay_div')]")));
		assertTrue(productWaiter.isDisplayed());		
		
	}
	 
	@Test
	public void ForgotPasswordLink(){
		NewUser newUser = new NewUser();
		join = driver.findElement(By.id("accountSpan"));
		join.click();
		SignIn_Register_CA regPage = PageFactory.initElements(driver, SignIn_Register_CA.class);
		WebElement linkWaiter = (new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(By.linkText("Forgot Your Password?")));
		assertTrue(linkWaiter.isDisplayed());
		regPage.ForgotPasswordProcedure(newUser);
		linkWaiter = (new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[contains(@id, 'WC_PasswordResetDisplay_div_')]")));
		assertTrue(linkWaiter.isDisplayed());
			
	}

	@Test
	public void CheckoutEmptyBag() throws IOException{
		
		HomePage_CA page = PageFactory.initElements(driver, HomePage_CA.class);
		signIn = driver.findElement(By.id("SignInLink"));
		new Actions(driver).moveToElement(signIn).click().perform();
		productWaiter = (new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(By.id("WC_AccountDisplay_links_2")));
		assertTrue(productWaiter.isDisplayed());
		TopBarPage_CA topBarPage = new TopBarPage_CA(driver);
		topBarPage.SignUp();
		productWaiter =  (new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(By.className("myaccount_desc_title")));
		topBarPage.ClickBagLink();
		productWaiter =  (new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[contains(@id, 'WC_EmptyShopCartDisplayf_div_')]")));
		assertTrue(productWaiter.isDisplayed());
		
		
		//This checks all the elements of an EMPTY Shopping Bag page
		assertTrue(driver.findElement(By.className("content_wrapper")).isDisplayed());
		assertTrue(driver.findElement(By.className("widget_breadcrumb_position")).isDisplayed());
		assertTrue(driver.findElement(By.xpath("//*[contains(@id, 'WC_EmptyShopCartDisplayf_div_')]")).isDisplayed());
		assertTrue(driver.findElement(By.id("promoCode")).isDisplayed());
		assertTrue(driver.findElement(By.className("button_secondary")).isDisplayed()); //Promo Code Apply button
		assertTrue(driver.findElement(By.xpath("//b[contains(text(), 'CAD $0.00')]")).isDisplayed());
		assertTrue(driver.findElement(By.className("question_section")).isDisplayed());
		assertTrue(driver.findElement(By.className("call")).isDisplayed());
		List<WebElement> questionLinks = driver.findElements(By.xpath("//*[contains(@id, 'WC_OrderItemDetailsf_links')]"));
		assertTrue(questionLinks.size() == 3); //This is the amount of links that have to be present inside the 'Questions' box
		assertTrue(driver.findElement(By.className("footer_wrapper")).isDisplayed());
			
	}
	
	
	@Test
	public void CheckoutModify() throws IOException, InterruptedException{
		sizesMultiplePurchase.add("facet_-M_1");
		sizesMultiplePurchase.add("facet_-M_1");
		
		SearchSingleProduct("Tribal Print Sweatshirt",0);
		pdpPage.SelectSizeAddToCart("1", sizesMultiplePurchase.get(0), PaymentMethod.CC, false, false , false);
		driver.navigate().back();
		WebElement waitLogin = (new WebDriverWait(driver, 60)).until(ExpectedConditions.elementToBeClickable(By.id("searchTotalCount")));
		
			try {
				
				 for(int i=1; i<sizesMultiplePurchase.size(); i++){
					 products = driver.findElements(By.className("product"));
					 new Actions(driver).moveToElement(products.get(i)).click().perform();
					 WebElement addtoCart = (new WebDriverWait(driver, 60)).until(ExpectedConditions.elementToBeClickable(By.className("current")));
					 pdpPage.SelectSizeAddToCart("1", sizesMultiplePurchase.get(i), PaymentMethod.CC, false, false , false);
					 driver.navigate().back();
					 waitLogin = (new WebDriverWait(driver, 60)).until(ExpectedConditions.elementToBeClickable(By.id("searchTotalCount")));
				 }
			
			
				
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		
		 topBarPage = new TopBarPage_CA(driver);
		topBarPage.ClickBagLink();
		productWaiter = (new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[contains(@id, 'shoppingCart_rowHeader_product')]")));
		System.out.println("");
		WebElement modify = driver.findElement(By.linkText("Modify"));
		new Actions(driver).moveToElement(modify).click().perform();
		
		assertTrue(driver.findElement(By.className("main_header")).isDisplayed()); // Product Title
		assertTrue(driver.findElement(By.xpath("//*[contains(@id, 'offerPrice_')]")).isDisplayed()); //Price Product
		assertTrue(driver.findElement(By.className("color_swatch")).isDisplayed()); //Color selected
		WebElement sizeQV = driver.findElement(By.className("selectedtext"));
		String sizeQV_txt = sizeQV.getText();
		
	}


	@Test
	public void EnterNoPromoCode() throws IOException{
		
		topBarPage = new TopBarPage_CA(driver);
		signIn = driver.findElement(By.id("SignInLink"));
		signIn.click();
		topBarPage.SignUp();
		
		productWaiter =  (new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(By.className("myaccount_desc_title")));
		topBarPage.ClickBagLink();
		
		productWaiter =  (new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(By.className("button_secondary")));
		productWaiter.click();
		assertTrue(driver.findElement(By.id("promo_error_meesage")).isDisplayed());
		System.out.println("");
		
	}
	
	
	@Test
	public void ValidPromoCode() throws IOException, InterruptedException{
		
		By topBarcart_lnk = By.id("minishopcart_total");
		WebElement updateLnk;
		WebElement cart;
		WebElement qty;
		WebElement promoCodeApply;
		WebElement imgDetailPromo;
		WebElement promUsed = null;
		WebElement cancelPromDetail;
		
		SearchSingleProduct("Tribal Print Sweatshirt",0);
		pdpPage.SelectSizeAddToCart("1", "swatch_-M_1", PaymentMethod.CC, false, false , false);
		
		productWaiter = ( new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(topBarcart_lnk));
	   
	    cart = driver.findElement(topBarcart_lnk);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", cart);
		productWaiter = (new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//*[contains(@id, 'qty')]"))));
		
		qty = driver.findElement(By.xpath("//*[contains(@id, 'qty')]"));
		qty.clear();
		qty.sendKeys("4");
		updateLnk = driver.findElement(By.xpath("//a[text()='Update']"));
		updateLnk.click();
		productWaiter = (new WebDriverWait(driver, 90)).until(ExpectedConditions.visibilityOf(driver.findElement(By.id("promoCode"))));
		productWaiter.sendKeys("FREESHIP1");
		promoCodeApply = driver.findElement(By.className("button_secondary"));
		promoCodeApply.click();
		
		assertTrue(driver.findElement(By.id("appliedPromotionCodes")).isDisplayed());
		
		productWaiter = (new WebDriverWait(driver, 90)).until(ExpectedConditions.visibilityOf(driver.findElement(By.id("promoCode"))));
		try{
			promUsed = driver.findElement(By.className("promotion_used"));
			
		}
		catch(Exception e){
			assertTrue(promUsed == null);
		}
		
		
		imgDetailPromo = driver.findElement(By.xpath(".//*[@id='promotion_1_details']/img"));
		imgDetailPromo.click();
		cancelPromDetail = driver.findElement(By.className("closeText"));
		assertTrue(cancelPromDetail.isEnabled());
		WebElement cancelPromo = driver.findElement(By.xpath("//a[contains(@id, 'promotion_')]"));
		cancelPromo.click();
		
	}
	
	
	@Test
	public void InvalidPromoCode() throws IOException{
		
		WebElement promoCodetxt;
		topBarPage = new TopBarPage_CA(driver);
		signIn = driver.findElement(By.id("SignInLink"));
		new Actions(driver).moveToElement(signIn).click().perform();
		topBarPage.SignUp();
		
		productWaiter =  (new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(By.className("myaccount_desc_title")));
		topBarPage.ClickBagLink();
		
		productWaiter =  (new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(By.className("button_secondary")));
		promoCodetxt = driver.findElement(By.id("promoCode"));
		promoCodetxt.sendKeys("THISISNOTACODE");
		productWaiter.click();
		assertTrue(driver.findElement(By.id("promo_error_meesage")).isDisplayed());
		System.out.println("");
		
	}
	
	@Test
	public void	CheckoutGuestUser_CanadaShipping() throws IOException, InterruptedException{
		
		WebElement guestEmail;
		WebElement submitGuestEmail;
		
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		
		SearchSingleProduct("Tribal Print Sweatshirt",0);
		pdpPage.SelectSizeAddToCart("1", "swatch_-M_1", PaymentMethod.CC, false, false , false);
		TopBarPage_CA topBarPage = new TopBarPage_CA(driver);
		WebElement topBarcart = driver.findElement(topBarPage.getBag_lnk());
		executor.executeScript("arguments[0].click();", topBarcart);
		
		WebElement checkout = (new WebDriverWait(driver, 60)).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='shopcartCheckout']/div")));
		executor.executeScript("arguments[0].click();", checkout);
		
		productWaiter = (new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(By.id("guestShopperContinue")));
		guestEmail = driver.findElement(By.className("guestSignInField"));
		guestEmail.sendKeys("randomUser@randomHost.com");
		productWaiter.click();
		
		productWaiter =(new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(By.id("saveShipAddressInput")));
		ShippingBillingPage_CA shipPage = PageFactory.initElements(driver, ShippingBillingPage_CA.class);
		assertTrue(productWaiter.isDisplayed());
		assertTrue(shipPage.ValidateCanadaShipping());
		assertTrue(shipPage.ValidaMandatoryShippingAddressFields());
		
	}
	
	
	@Test
	public void Checkout_BillingAddress() throws IOException, InterruptedException{
		WebElement guestEmail;
		WebElement submitGuestEmail;
		
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		
		SearchSingleProduct("Tribal Print Sweatshirt",0);
		pdpPage.SelectSizeAddToCart("1", "swatch_-M_1", PaymentMethod.CC, false, false , false);
		TopBarPage_CA topBarPage = new TopBarPage_CA(driver);
		WebElement topBarcart = driver.findElement(topBarPage.getBag_lnk());
		executor.executeScript("arguments[0].click();", topBarcart);
		
		WebElement checkout = (new WebDriverWait(driver, 60)).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='shopcartCheckout']/div")));
		executor.executeScript("arguments[0].click();", checkout);
		
		productWaiter = (new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(By.id("guestShopperContinue")));
		guestEmail = driver.findElement(By.className("guestSignInField"));
		guestEmail.sendKeys("randomUser@randomHost.com");
		productWaiter.click();
		
		productWaiter =(new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(By.id("saveShipAddressInput")));
		ShippingBillingPage_CA shipPage = PageFactory.initElements(driver, ShippingBillingPage_CA.class);
		assertTrue(productWaiter.isDisplayed());
		
		shipPage.CreateShippingAddress();
		productWaiter = (new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(By.id("shopcartCheckout")));
		productWaiter.click();

		productWaiter = (new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(By.id("deliveryMethodRadio")));
		shipPage.getBtnContinue().click();
		productWaiter  = (new WebDriverWait(driver, 90)).until(ExpectedConditions.elementToBeClickable(By.id("sameAsShippingRadio")));
		productWaiter.click();
		WebElement sameAddress = driver.findElement(By.id("billingAddressDisplayArea"));
		assertTrue(sameAddress.getText().length()>0);
		
		
		
		
	}
	
	
	
	
}



