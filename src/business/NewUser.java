package business;



public class NewUser extends BaseBusinessPage{

	private String firstName;
	private String lastName;
	private String email;
	private String confirmEmail;
	private String password;
	private String confirmPassword;
	private String DayOfBirth;
	private String MonthOfBirth;
	private String YearOfBirth;
	private String resetMail;
	
	public NewUser(){
		super();
		firstName = business_data.getProperty("first_name").toString();
		lastName = business_data.getProperty("last_name").toString();
		email = business_data.getProperty("email").toString();
		confirmEmail = business_data.getProperty("confirm_email").toString();
		password = business_data.getProperty("password").toString();
		confirmPassword = business_data.getProperty("confirm_password").toString();
		DayOfBirth = business_data.getProperty("dob").toString();
		MonthOfBirth = business_data.getProperty("mob").toString();
		YearOfBirth = business_data.getProperty("yob").toString();
		resetMail = business_data.getProperty("reset_email").toString();
	}

	public String getResetMail(){
		return resetMail;
	}
	
	public String getFirstName() {
		return firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public String getEmail() {
		return email;
	}
	public String getConfirEmail() {
		return confirmEmail;
	}
	public String getPassword() {
		return password;
	}
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public String getDOB() {
		return DayOfBirth;
	}
	public String getMOB() {
		return MonthOfBirth;
	}
	public String getYOB() {
		return YearOfBirth;
	}
	
	
	



	
}
